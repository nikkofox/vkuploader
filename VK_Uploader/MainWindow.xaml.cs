﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using VkNet;
using WinForms = System.Windows.Forms;
using SD = System.Drawing;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Documents;
using System.Text.RegularExpressions;

namespace VK_Uploader
{
    delegate void UpdateProgressBarDelegate(DependencyProperty dp, object value);

    
    public partial class MainWindow : Window
    {
        VkApi vk = new VkApi();
        BackgroundWorker worker;
        DirectoryInfo di = Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "VK_Uploader"));
        string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "VK_Uploader", "token");
        string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "VK_Uploader", "lastPath");
        public static ProgressBar ProgressBarImg;
        public static Label UploadPerc;
        public MainWindow()
        {
#if DEBUG
            System.Diagnostics.PresentationTraceSources.DataBindingSource.Switch.Level = System.Diagnostics.SourceLevels.Critical;
#endif
            InitializeComponent();
            ProgressBarImg = ProgressBarImage;
            UploadPerc = UploadPercent;
            DeleteToken.Visibility = Visibility.Collapsed;
            if (File.Exists(filePath))
            {
                try
                {
                    string[] path = File.ReadAllLines(filePath);
                    txtPath.Text = path[0];
                    MonitorDirectory(txtPath.Text);
                    txtPath.Foreground = new SolidColorBrush(Colors.Black);
                }
                catch
                {
                    txtPath.Text = AppDomain.CurrentDomain.BaseDirectory;
                }
            }
            else
            {
                txtPath.Text = AppDomain.CurrentDomain.BaseDirectory;
            }
            if (File.Exists(fileName))
            {
                try
                {
                    string[] token = File.ReadAllLines(fileName);
                    VK.VK_Authorize("", "", vk, token[0]);
                    Disable_and_LoadGroups();
                }
                catch
                {
                    MessageBox.Show("Ошибка авторизации по сохранённым данным\n" +
                        "Авторизуйтесь по логину и паролю", "Ошибка",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    File.Delete(fileName);
                }
            }
        }
        private void ButtonAuthorize_Click(object sender, RoutedEventArgs e)
        {
            string login = LoginBox.Text;
            string password = PwdBox.Password.ToString();
            try
            {
                string token;
                try
                {
                    token = VK.VK_Authorize(login, password, vk);
                }
                catch
                {
                    token = VK.VK_AuthorizeTwoFactor(login, password, vk);
                }
                if (token != "")
                {
                    if (SaveAcc.IsChecked == true)
                    {
                        using (StreamWriter sw = new StreamWriter(fileName))
                        {
                            sw.Write(vk.Token);
                        }
                    }
                    Disable_and_LoadGroups();
                }
            }
            catch
            {
                MessageBox.Show("Ошибка авторизации\n" +
                "Проверьте логин и пароль и повторите попытку", "Ошибка",
                   MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private static String[] GetFilesFrom(String searchFolder, String[] filters, bool isRecursive)
        {
            List<String> filesFound = new List<String>();
            var searchOption = isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            foreach (var filter in filters)
            {
                filesFound.AddRange(Directory.GetFiles(searchFolder, String.Format("*.{0}", filter), searchOption));
            }
            return filesFound.ToArray();
        }
        private void Disable_and_LoadGroups()
        {
            LoginBox.Visibility = Visibility.Collapsed;
            PwdBox.Visibility = Visibility.Collapsed;
            btnAuthorize.Visibility = Visibility.Collapsed;
            SaveAcc.Visibility = Visibility.Collapsed;
            lbl1.Visibility = Visibility.Collapsed;
            lbl2.Visibility = Visibility.Collapsed;
            DeleteToken.Visibility = Visibility.Visible;
            btnSelectFolder.IsEnabled = true;
            try
            {
                var groups = VK.Groups_Get(vk);
                foreach (var g in groups)
                {
                    string name = g.Name;
                    ListGroups.Items.Add(new ListBoxItem() { Content = g.Name, Tag = g.Id });
                }
            }
            catch
            {
                MessageBox.Show("Ошибка авторизации по сохранённым данным\n" +
                    "Авторизуйтесь по логину и паролю", "Ошибка",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                LoginBox.Visibility = Visibility.Visible;
                PwdBox.Visibility = Visibility.Visible;
                btnAuthorize.Visibility = Visibility.Visible;
                SaveAcc.Visibility = Visibility.Visible;
                lbl1.Visibility = Visibility.Visible;
                lbl2.Visibility = Visibility.Visible;
                DeleteToken.Visibility = Visibility.Collapsed;
                btnSelectFolder.IsEnabled = false;
                StartUpload.IsEnabled = false;
                File.Delete(fileName);
            }

        }
        private void BtnSelectFolder_Click(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog folderDialog = new WinForms.FolderBrowserDialog
            {
                ShowNewFolderButton = false,
                SelectedPath = txtPath.Text
            };
            WinForms.DialogResult result = folderDialog.ShowDialog();

            if (result == WinForms.DialogResult.OK)
            {
                String sPath = folderDialog.SelectedPath;
                txtPath.Foreground = new SolidColorBrush(Colors.Black);
                txtPath.Text = sPath;
                MonitorDirectory(txtPath.Text);
                using (StreamWriter sw = new StreamWriter(filePath))
                {
                    sw.Write(sPath);
                }
            }


        }

        private void MonitorDirectory(string path)
        {
            var filters = new String[] { "jpg", "jpeg", "png", "gif", "bmp" };
            var files = GetFilesFrom(txtPath.Text, filters, false);
            LeftImg.Content = files.Length;
            FileSystemWatcher fileSystemWatcher = new FileSystemWatcher
            {
                Path = path
            };
            fileSystemWatcher.Changed += FileSystemWatcher_Changed;
            fileSystemWatcher.Created += FileSystemWatcher_Changed;
            fileSystemWatcher.Deleted += FileSystemWatcher_Changed;
            fileSystemWatcher.EnableRaisingEvents = true;
        }

        private void FileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            string path = null;
            Dispatcher.Invoke(() => path = txtPath.Text);
            var filters = new String[] { "jpg", "jpeg", "png", "gif", "bmp" };
            var files = GetFilesFrom(path, filters, false);
            Dispatcher.Invoke(new Action(() => { LeftImg.Content = files.Length; }));
        }


        private void ListGroups_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ListGroups.SelectedItem != null)
            {
                StartUpload.IsEnabled = false;
                ListAlbums.SelectedIndex = -1;
                LblPhotosCount.Content = "";
                int index = ListGroups.SelectedIndex;
                long id = (long)((ListBoxItem)ListGroups.Items[ListGroups.SelectedIndex]).Tag;
                try
                {
                    List<ListBoxItem> itemsSource = new List<ListBoxItem>();
                    var albums = VK.PhotoAlbums_Get(-id, vk);
                    foreach (var a in albums)
                    {
                        itemsSource.Add(new ListBoxItem() { Content = a.Title, Tag = a });
                    }
                    ListAlbums.ItemsSource = itemsSource;
                    CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(ListAlbums.ItemsSource);
                    view.Filter = CustomFilter;
                }
                catch
                {
                    MessageBox.Show("Неизвестная ошибка\n" +
                    "Выберите другую группу", "Ошибка",
                       MessageBoxButton.OK, MessageBoxImage.Error);
                    Progress.Value = 0;
                    LabelStatusUpload.Foreground = Brushes.DarkRed;
                    LabelStatusUpload.Content = "Произошла ошибка";
                }

            }
        }


        private void DeleteToken_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("При следующем запуске программы\nВам снова будет необходимо авторизоваться используя логин и пароль",
                                          "Данные авторизации удалены",
                                          MessageBoxButton.OK,
                                          MessageBoxImage.Information);
            DeleteToken.IsEnabled = false;
            File.Delete(fileName);
        }


        private void StartUpload_Click(object sender, RoutedEventArgs e)
        {
            CountImg.Content = "";
            LabelStatusUpload.Foreground = Brushes.DarkOrange;
            LabelStatusUpload.Content = "Идёт загрузка";
            long group_id = (long)((ListBoxItem)ListGroups.Items[ListGroups.SelectedIndex]).Tag;
            VkNet.Model.PhotoAlbum album = (VkNet.Model.PhotoAlbum)((ListBoxItem)ListAlbums.Items[ListAlbums.SelectedIndex]).Tag;
            long album_id = album.Id;
            var filters = new String[] { "jpg", "jpeg", "png", "gif", "bmp" };
            try
            {
                var files = GetFilesFrom(txtPath.Text, filters, false);
                if (files.Length != 0 && (album.Size + files.Length) <= 10000)
                {
                    string uploadedPath = Path.Combine(txtPath.Text, "_VKUploaded");
                    if (DelPhotoCheck.IsChecked == false)
                    {
                        Directory.CreateDirectory(uploadedPath);
                    }
                    worker = new BackgroundWorker();
                    List<object> arguments = new List<object>

                    {
                            group_id,
                            album_id,
                            files,
                            uploadedPath,
                            vk
                    };
                    Mass_IsEnabled(false);
                    BarProgress.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Normal;
                    Progress.Maximum = files.Length;
                    Progress.Value = 0;
                    worker.DoWork += new DoWorkEventHandler(Upload_Images);
                    worker.RunWorkerCompleted += Upload_Complete;
                    worker.WorkerSupportsCancellation = true;
                    worker.RunWorkerAsync(arguments);
                }
                else if ((album.Size + files.Length) > 10000)
                {
                    MessageBox.Show("Невозможно загрузить в этот фотоальбом!\n" +
                        "Возможно в фотоальбоме уже максимальное количество изображений. " +
                        "\n\n" +
                        "Поробуйте уменьшить количество фото, либо выбрать другой альбом.", "Ошибка",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    Progress.Value = 0;
                    LabelStatusUpload.Foreground = Brushes.DarkRed;
                    LabelStatusUpload.Content = "Произошла ошибка";
                }
                else
                {
                    MessageBox.Show("В папке не найдено изображений", "Ошибка",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    Progress.Value = 0;
                    LabelStatusUpload.Foreground = Brushes.DarkRed;
                    LabelStatusUpload.Content = "Произошла ошибка";
                }
            }
            catch
            {
                MessageBox.Show("Что-то не так с папкой\n" +
                    "Попробуйте выбрать другую", "Ошибка",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                Progress.Value = 0;
                LabelStatusUpload.Foreground = Brushes.DarkRed;
                LabelStatusUpload.Content = "Произошла ошибка";
            }
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void Upload_Images(object sender, DoWorkEventArgs e)
        {
            UpdateProgressBarDelegate updProgress = new UpdateProgressBarDelegate(Progress.SetValue);
            Random rand = new Random();
            double value = 0;
            List<object> genericlist = e.Argument as List<object>;
            long group_id = (long)genericlist[0];
            long album_id = (long)genericlist[1];
            var files = (string[])genericlist[2];
            string uploadedPath = (string)genericlist[3];
            var vk = (VkApi)genericlist[4];
            int c = 1;
            double percent = 100D / files.Length;
            double process = percent;
            bool? isDelStatus = false;
            DelPhotoCheck.Dispatcher.Invoke(new Action(() => isDelStatus = DelPhotoCheck.IsChecked));
            string userPause = "0";
            foreach (var i in files)
            {
                textPause.Dispatcher.Invoke(new Action(() => userPause = textPause.Text));
                if (string.IsNullOrWhiteSpace(userPause))
                {
                    userPause = "0";
                }

                Application.Current.Dispatcher.BeginInvoke(new Action(() => { CountImg.Content = c + "/" + files.Length; BarProgress.ProgressValue = process / 100D; }));
                Application.Current.Dispatcher.Invoke(updProgress, new object[] { ProgressBar.ValueProperty, ++value });
                process += percent;
                FileInfo file = new FileInfo(i);
                var sizeInBytes = file.Length;
                SD.Bitmap bmp = new SD.Bitmap(i);
                int w = bmp.Size.Width;
                int h = bmp.Size.Height;
                byte[] imgBytes;
                if (w + h > 11000 || sizeInBytes > 40000000 || w > 6000 || h > 6000)
                {
                    if (w > h || w > 6000) imgBytes = Resize_Picture(bmp, 3000, 0, 95);
                    else imgBytes = Resize_Picture(bmp, 0, 3000, 95);
                }
                else { imgBytes = ImageToByteArray(bmp); }
                bmp.Dispose();
                string caption = new TextRange(textDescription.Document.ContentStart, textDescription.Document.ContentEnd).Text;
                VK.Upload_Photo(imgBytes, group_id, album_id, caption, vk);
                c++;
                try
                {
                    if (isDelStatus == true) File.Delete(i);
                    else File.Move(i, Path.Combine(uploadedPath, Path.GetFileName(i)));
                }
                catch (IOException)
                {

                }
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    return;
                }
                Application.Current.Dispatcher.Invoke(delegate { ProgressBarImg.Value = 0; UploadPerc.Content = "0%"; });
                System.Threading.Thread.Sleep((1000 * Convert.ToInt32(userPause)) + rand.Next(50, 700));
            }
        }

        private void Upload_Complete(object sender, RunWorkerCompletedEventArgs e)
        {
            var selectAlbum = ListAlbums.SelectedIndex;
            int index = ListGroups.SelectedIndex;
            long id = (long)((ListBoxItem)ListGroups.Items[ListGroups.SelectedIndex]).Tag;
            try
            {
                List<ListBoxItem> itemsSource = new List<ListBoxItem>();
                var albums = VK.PhotoAlbums_Get(-id, vk);
                foreach (var a in albums)
                {
                    itemsSource.Add(new ListBoxItem() { Content = a.Title, Tag = a });
                }
                ListAlbums.ItemsSource = itemsSource;
                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(ListAlbums.ItemsSource);
                view.Filter = CustomFilter;
            }
            catch
            {

            }
            if (e.Cancelled)
            {
                if (MessageBox.Show("Загрузка успешно остановлена",
                                          "Отменить",
                                          MessageBoxButton.OK,
                                          MessageBoxImage.Information) == MessageBoxResult.OK)
                {
                    ListAlbums.SelectedIndex = selectAlbum;
                    Mass_IsEnabled(true);
                    Progress.Value = 0;
                    ProgressBarImg.Value = 0;
                    TaskbarItemInfo.ProgressValue = 0;
                    UploadPerc.Content = "";
                    LabelStatusUpload.Foreground = Brushes.DarkCyan;
                    LabelStatusUpload.Content = "Загрузка остановлена";
                }
            }
            else
            {
                if (e.Error != null)
                {
                    System.Media.SystemSounds.Hand.Play();
                    //TaskbarItemInfo.ProgressValue = 0;
                    TaskbarItemInfo.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Error;
                    BtnCancel.IsEnabled = false;
                    ListAlbums.SelectedIndex = selectAlbum;
                    Mass_IsEnabled(true);
                    Progress.Value = 0;
                    ProgressBarImg.Value = 0;
                    UploadPerc.Content = "";
                    LabelStatusUpload.Foreground = Brushes.DarkRed;
                    LabelStatusUpload.Content = "Произошла ошибка";
                }
                else
                {
                    System.Media.SystemSounds.Beep.Play();
                    TaskbarItemInfo.ProgressValue = 0;
                    BtnCancel.IsEnabled = false;
                    ListAlbums.SelectedIndex = selectAlbum;
                    Mass_IsEnabled(true);
                    Progress.Value = 0;
                    ProgressBarImg.Value = 0;
                    UploadPerc.Content = "";
                    LabelStatusUpload.Foreground = Brushes.DarkGreen;
                    LabelStatusUpload.Content = "Успешно загружено";
                }
            }
        }

        private void Mass_IsEnabled(bool status)
        {
            StartUpload.IsEnabled = status;
            ListGroups.IsEnabled = status;
            ListAlbums.IsEnabled = status;
            btnSelectFolder.IsEnabled = status;
            DelPhotoCheck.IsEnabled = status;
            textDescription.IsEnabled = status;
            addDescription.IsEnabled = status;
            FindAlbum.IsEnabled = status;
            BtnCancel.IsEnabled = !status;
        }
        private static byte[] Resize_Picture(SD.Bitmap bmp, int FinalWidth, int FinalHeight, int ImageQuality)
        {
            SD.Bitmap NewBMP;
            SD.Graphics graphicTemp;
            int iWidth;
            int iHeight;
            if ((FinalHeight == 0) && (FinalWidth != 0))
            {
                iWidth = FinalWidth;
                iHeight = (bmp.Size.Height * iWidth / bmp.Size.Width);
            }
            else if ((FinalHeight != 0) && (FinalWidth == 0))
            {
                iHeight = FinalHeight;
                iWidth = (bmp.Size.Width * iHeight / bmp.Size.Height);
            }
            else
            {
                iWidth = FinalWidth;
                iHeight = FinalHeight;
            }

            NewBMP = new SD.Bitmap(iWidth, iHeight);
            graphicTemp = SD.Graphics.FromImage(NewBMP);
            graphicTemp.CompositingMode = SD.Drawing2D.CompositingMode.SourceOver;
            graphicTemp.CompositingQuality = SD.Drawing2D.CompositingQuality.HighQuality;
            graphicTemp.SmoothingMode = SD.Drawing2D.SmoothingMode.HighQuality;
            graphicTemp.InterpolationMode = SD.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphicTemp.PixelOffsetMode = SD.Drawing2D.PixelOffsetMode.HighQuality;
            graphicTemp.DrawImage(bmp, 0, 0, iWidth, iHeight);
            graphicTemp.Dispose();
            SD.Imaging.EncoderParameters encoderParams = new SD.Imaging.EncoderParameters();
            SD.Imaging.EncoderParameter encoderParam = new SD.Imaging.EncoderParameter(SD.Imaging.Encoder.Quality, ImageQuality);
            encoderParams.Param[0] = encoderParam;
            SD.Imaging.ImageCodecInfo[] arrayICI = SD.Imaging.ImageCodecInfo.GetImageEncoders();
            using (var ms = new MemoryStream())
            {
                for (int fwd = 0; fwd <= arrayICI.Length - 1; fwd++)
                {
                    if (arrayICI[fwd].FormatDescription.Equals("JPEG"))
                    {
                        NewBMP.Save(ms, arrayICI[fwd], encoderParams);
                    }
                }
                NewBMP.Dispose();
                return ms.ToArray();
            }
        }


        private byte[] ImageToByteArray(SD.Bitmap imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }


        private void ListAlbums_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListAlbums.SelectedIndex != -1)
            {
                VkNet.Model.PhotoAlbum album = (VkNet.Model.PhotoAlbum)((ListBoxItem)ListAlbums.Items[ListAlbums.SelectedIndex]).Tag;
                LblPhotosCount.Content = "Фотографий в альбоме: " + album.Size;
                StartUpload.IsEnabled = true;
            }
            else StartUpload.IsEnabled = false;

        }


        private void Progress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
        private void ProgressBarImage_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите отменить загрузку?", "Отменить",
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                worker.CancelAsync();
                BtnCancel.IsEnabled = false;
            }

        }

        private bool CustomFilter(object obj)
        {
            if (string.IsNullOrEmpty(FindAlbum.Text) && ListAlbums.Items.Count == 0)
            {
                return true;
            }
            else
            {
                return (obj.ToString().IndexOf(FindAlbum.Text, StringComparison.OrdinalIgnoreCase) >= 0);
            }
        }

        private void FindAlbum_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ListAlbums.IsEnabled)
            {
                CollectionViewSource.GetDefaultView(ListAlbums.ItemsSource).Refresh();
            }
        }

        private void Description_Checked(object sender, RoutedEventArgs e)
        {
            textDescription.Visibility = Visibility.Visible;
        }

        void ClickNameCopy(Object sender, RoutedEventArgs args)
        {
            if (ListAlbums.SelectedItem is ListBoxItem mySelectedItem)
            {
                Clipboard.SetText(mySelectedItem.Content.ToString());
            }
        }
        void ClickUrlCopy(Object sender, RoutedEventArgs args)
        {
            if (ListAlbums.SelectedItem is ListBoxItem)
            {
                long group_id = (long)((ListBoxItem)ListGroups.Items[ListGroups.SelectedIndex]).Tag;
                VkNet.Model.PhotoAlbum album = (VkNet.Model.PhotoAlbum)((ListBoxItem)ListAlbums.Items[ListAlbums.SelectedIndex]).Tag;
                Clipboard.SetText(String.Format("https://vk.com/album-{0}_{1}", group_id, album.Id));
            }
        }
        void ClickCut(Object sender, RoutedEventArgs args) { MessageBox.Show("Right Click"); }
        void ClickSelectAll(Object sender, RoutedEventArgs args) { MessageBox.Show("Right Click"); }

        private void Description_Unchecked(object sender, RoutedEventArgs e)
        {
            textDescription.Document.Blocks.Clear();
            textDescription.Visibility = Visibility.Collapsed;
        }

        
    }
}